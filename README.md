# Godot parallax shader
Can be used for red dots, holo sights and fighter jet heads up displays (HUD).

![](.gitlab/reflex.webm)

# Features
* Infinite distance
* Rotates with the object it's attached to
* Cross-platform

# Shader
If you are only interested in the shader, check it out [here][shader-link].

# Project
If you want to see the shader in action, clone this project and run it. Tested with Godot 3.2.
```bash
# Get the source code and dependencies
$ git clone https://gitlab.com/MrMinimal/godot-reflex-sight
```

---

# Credits
This project is built using open-source technologies only.
Main components include but are not limited to:

| **Name**  | **Usage**         | **License** | **Website** | **Credit** |
|-----------|-------------------|-------------|-------------|-------------|
| Godot     | game engine       | MIT         | https://godotengine.org | - |
| vazgriz/ReflexSight     | Shader in Unity       | MIT         | https://github.com/vazgriz/ReflexSight | https://github.com/vazgriz |
| Blender   | 3D asset creation | GPL-2.0+    | https://blender.org | - |
| Gun Model  | Display of shader | CC BY-NC 4.0 | https://sketchfab.com/3d-models/fn-fnx-45-892642770a214d9da06b57a68827175f | https://sketchfab.com/Lagzor |
| Godot demo projects     | background scenery       | MIT         | https://github.com/godotengine/godot-demo-projects | - |

# License
The shader code is licensed under AGPL-3.0+.
Assets of this repo vary in license, see the Credits above for reference.

[shader-link]: https://gitlab.com/MrMinimal/godot-reflex-sight/-/blob/master/shader/HudShader.shader


